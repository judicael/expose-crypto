TEXINPUTS='.//:'
LATEXFLAGS='-synctex=1  -file-line-error'
LATEX=pdflatex $LATEXFLAGS

dest=/home/judicael/notbackuped/judicael.courant.free.fr/2013/05
all:V: crypto-slides.finished.pdf

# install:V:
#   mkdir -p $dest
#   cp poo-slides.pdf $dest
#   weex judicael.courant.free.fr

crypto-slides.pdf: properties.tex config.tex crypto-body.tex macros.tex

Livre.pdf: Livre.tex
  TEXINPUTS='.//:' xelatex Livre.tex

%.pdf: %.tex
  $LATEX $stem < /dev/null

%.finished.pdf: %.pdf
  cp $stem.synctex.gz $stem.finished.synctex.gz || true
  cp $stem.pdf $target
